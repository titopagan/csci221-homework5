#ifndef MATH_TREE_H
#define MATH_TREE_H

#include <string>

struct asciinode_struct;

class Tree
{
public:
    Tree();
    Tree(double val);
    Tree(char var);
    Tree(std::string op, Tree *left, Tree *right);
    Tree *derive();
    Tree *simplify();
    bool equals(Tree *other);
    void print_preorder();
    bool is_var();
    bool is_val();
    bool is_unary_op();
    bool is_binary_op();
    Tree *clone();
    ~Tree();
	void recursiveDelete();
private:
    Tree *simplify_rec();
    Tree *left;
    Tree *right;
    std::string op;
    double val;
    char var;

    friend void print_ascii_tree(Tree *t);
    friend asciinode_struct *build_ascii_tree_recursive(Tree *t);
    friend asciinode_struct *build_ascii_tree(Tree *t);
};

#endif
