#include <cmath>
#include <iostream>
#include <cmath>
#include "mathtree.h"
using namespace std;

Tree::Tree()
{
    left = NULL;
    right = NULL;
    op = "";
    val = 0.0;
    var = 0;
}

Tree::Tree(double val)
{
    left = NULL;
    right = NULL;
    op = "";
    this->val = val;
    var = 0;
}

Tree::Tree(char var)
{
    left = NULL;
    right = NULL;
    op = "";
    val = 0.0;
    this->var = var;
}

Tree::Tree(string op, Tree *left, Tree *right)
{
    this->left = left;
    this->right = right;
    this->op = op;
    val = 0.0;
    var = 0;
}

bool Tree::is_var()
{
    return (op == "" && left == NULL && right == NULL && var != 0);
}

bool Tree::is_val()
{
    return (op == "" && left == NULL && right == NULL && var == 0);
}

bool Tree::is_unary_op()
{
    return (op != "" && left != NULL && right == NULL);
}

bool Tree::is_binary_op()
{
    return (op != "" && left != NULL && right != NULL);
}

Tree::~Tree()
{
    recursiveDelete();
}

void Tree::recursiveDelete()
{
	Tree* temp1 = this->left;
    Tree* temp2 = this->right;
    if(temp1 != NULL)
    {
		temp1->recursiveDelete();
        delete temp1;
    }
    if(temp2 != NULL)
    {
		temp2->recursiveDelete();
        delete temp2;
    }
}

bool Tree::equals(Tree *other)
{
    if(is_val() && other->is_val() && val == other->val) { return true; }
    if(is_var() && other->is_var() && var == other->var) { return true; }
    if(is_unary_op() && other->is_unary_op() && op == other->op && left->equals(other->left)) { return true; }
    if(is_binary_op() && other->is_binary_op() && op == other->op && left->equals(other->left) && right->equals(other->right)) { return true; }
    return false;
}

Tree *Tree::clone()
{
    Tree *top = NULL;
    if(is_var())
    {
        top = new Tree(var);
    }
    else if(is_val())
    {
        top = new Tree(val);
    }
    else if(is_binary_op())
    {
        Tree *new_left = left->clone();
        Tree *new_right = right->clone();
        top = new Tree(op, new_left, new_right);
    }
    else if(is_unary_op())
    {
        Tree *new_left = left->clone();
        top = new Tree(op, new_left, NULL);
    }
    return top; 
}

Tree *Tree::simplify_rec()
{
    Tree *priortree = clone();
	
	if(priortree->is_val())
	{
		return priortree;
	}
	
	if(priortree->is_binary_op())
	{
		if(priortree->op == "+")
		{
			if(priortree->left->is_val() && (priortree->left->val == 0.0 && (priortree->right->is_val() || priortree->right->is_var())))
			{
				priortree->op = "";
				priortree->val = priortree->right->val;
				priortree->var = priortree->right->var;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->right->is_val() && ((priortree->left->is_val() || priortree->left->is_var()) && priortree->right->val == 0.0))
			{
				priortree->op = "";
				priortree->val = priortree->left->val;
				priortree->var = priortree->left->var;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->left->is_val() && priortree->right->is_val())
			{
				double answer = priortree->left->val + priortree->right->val;
				
				priortree->op = "";
				priortree->val = answer;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
					
				priortree->left = NULL;
				priortree->right = NULL;
			}
		}
		else if(priortree->op == "*")
		{
			if(priortree->left->is_val() && priortree->left->val == 1.0 && (priortree->right->is_val() || priortree->right->is_var()))
			{
				cout<<"in simplify 1" << endl;
				
				Tree* tmp = priortree->right->clone();
				
				priortree = tmp;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
				
				/*priortree->op = "";
				priortree->val = priortree->right->val;
				priortree->var = priortree->right->var;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;*/
			}
			else if(priortree->right->is_val() && priortree->right->val == 1.0 && (priortree->left->is_val() || priortree->left->is_var()))
			{
				cout<<"in simplify 2" << endl;
				
				Tree* tmp = priortree->left->clone();
				
				priortree = tmp;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
				
				/*priortree->op = "";
				priortree->val = priortree->left->val;
				priortree->var = priortree->left->var;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;*/
			}
			else if(priortree->left->is_val() && (priortree->left->val == 0.0 && (priortree->right->is_val() || priortree->right->is_var())))
			{
				priortree->op = "";
				priortree->val = 0.0;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if((priortree->right->is_val() && ((priortree->left->is_val() || priortree->left->is_var() ) && priortree->right->val == 0.0)))
			{
				priortree->op = "";
				priortree->val = 0.0;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->left->is_val() && priortree->right->is_val())
			{
				double answer = priortree->left->val * priortree->right->val;
				
				priortree->op = "";
				priortree->val = answer;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
					
				priortree->left = NULL;
				priortree->right = NULL;
			}
		}
		else if(priortree->op == "-")
		{
			if(priortree->right->is_val() && priortree->right->val == 0.0)
			{
				Tree *tmp = left->clone();
				
				delete priortree;
				
				priortree = tmp;
			}
			else if(priortree->left->is_val() && priortree->right->is_val())
			{
				double answer = priortree->left->val - priortree->right->val;
				
				priortree->op = "";
				priortree->val = answer;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
		}
		else if(priortree->op == "/")
		{
			if(priortree->right->is_val() && (priortree->left->is_val() == 0 && priortree->right->val == 0))
			{
				priortree->op = "";
				priortree->val = 0;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->left->is_val() && priortree->right->is_val())
			{
				double answer = priortree->left->val + priortree->right->val;
				
				priortree->op = "";
				priortree->val = answer;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
					
				priortree->left = NULL;
				priortree->right = NULL;
			}
		}
		else if(priortree->op == "^")
		{
			if(priortree->left->is_val() && priortree->right->is_val())
			{
				double answer = pow(priortree->left->val, priortree->right->val);
			
				priortree->op = "";
				priortree->val = answer;
				priortree->var = 0;
				
				delete priortree->left;
				delete priortree->right;
					
				priortree->left = NULL;
				priortree->right = NULL;
			}		
		}
	}

	else if(priortree->is_unary_op())
	{
		if(priortree->right != NULL )
		{
			if(priortree->op == "ln" && priortree->right->is_val())
			{
				priortree->op = "";
				priortree->val = (double)log(priortree->right->val);
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->op == "sin" && priortree->right->is_val())
			{
				priortree->op = "";
				priortree->val = (double)sin(priortree->right->val);
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->op == "cos" && priortree->right->is_val())
			{
				priortree->op = "";
				priortree->val = (double)cos(priortree->right->val);
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
		}
		else if(priortree->left != NULL)
		{
			if(priortree->op == "ln" && priortree->left->is_val())
			{
				priortree->op = "";
				priortree->val = (double)log(priortree->left->val);
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->op == "sin" && priortree->left->is_val())
			{
				priortree->op = "";
				priortree->val = (double)sin(priortree->left->val);
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
			else if(priortree->op == "cos" && priortree->left->is_val())
			{
				priortree->op = "";
				priortree->val = (double)cos(priortree->left->val);
				
				delete priortree->left;
				delete priortree->right;
				
				priortree->left = NULL;
				priortree->right = NULL;
			}
		}
	}
	
	if(priortree->left != NULL)
	{
		Tree* tmp = priortree->left;
		priortree->left = priortree->left->simplify_rec();
		delete tmp;
	}
	if(priortree->right != NULL)
	{
		Tree* tmp = priortree->right;
		priortree->right = priortree->right->simplify_rec();
		delete tmp;
	}

    return priortree;
}

Tree *Tree::simplify()
{
    Tree *priortree = clone();
    Tree *newtree = priortree->simplify_rec();

    // simplify over-and-over until the output is the same
    while(!priortree->equals(newtree))
    {
        delete priortree;
        priortree = newtree;
        newtree = priortree->simplify_rec();
    }
    delete priortree;
	
    return newtree;
}

Tree *Tree::derive()
{
    Tree *priortree = clone();
	double foo = 0.0;
	char bar  = 0;
	
	if(priortree->is_var())
	{
		priortree->var = 0;
		priortree->val = 1.0;
	}
	else if(priortree->is_val())
	{
		priortree->val = 0.0;
	}
	else if(priortree->is_unary_op())
	{
		if(priortree->op == "sin")
		{
			Tree *newCos = new Tree("cos", priortree->left, NULL);
			Tree *result = new Tree("*",newCos,left->derive());
			delete priortree;
			return result;
		}
		else if(priortree->op == "cos")
		{
			Tree *newSin = new Tree("sin", priortree->left, NULL);
			Tree *negative = new Tree("-",new Tree(0.0),newSin);
			Tree *result = new Tree("*",negative,left->derive());
			delete priortree;
			return result;
		}
		// I'm fucked up, fix me
		else if(priortree->op == "ln")
		{
			Tree *one = new Tree(1.0);
			Tree *inside = priortree->left->clone();
			Tree *lnDerive = new Tree("/", one, inside);
			delete priortree;
			return lnDerive;
		}
	}
	
	else if(priortree->is_binary_op())
	{
		if(priortree->op == "+")
		{
			Tree *addingDerive = new Tree("+", priortree->left->derive(), priortree->right->derive());
			delete priortree;
			return addingDerive;
		}
		else if(priortree->op == "-")
		{
			Tree *subbingDerive = new Tree("-", priortree->left->derive(), priortree->right->derive());
				delete priortree;
			return subbingDerive;
		}
		else if(priortree->op == "*")
		{
			Tree *first = new Tree("*", left->derive(), right->derive());
			Tree *second = new Tree("*", left->clone(),right->derive());
			Tree *third = new Tree("+", first, second);
			delete priortree;
			return third;
		}
		else if(priortree->op == "/")
		{
			Tree *another = new Tree(2.0);
			Tree *first = new Tree("*", left->derive(), right->clone());
			Tree *second = new Tree("*", right->derive(),left->clone());
			Tree *third = new Tree("-", first, second);
			Tree *fourth = new Tree("^", right->clone(), another);
			Tree *fifth = new Tree("/", third, fourth);
			
			/*delete clone2;
			delete another;*/
			delete priortree;
			return fifth;
		}
		else if(priortree->op == "^")
		{
			Tree *ln = new Tree("ln", left->clone(), NULL);
			Tree *first = new Tree("^", left->clone(), right->clone());
			Tree *second = new Tree("/", priortree->right->clone(), left->clone());
			Tree *third = new Tree("*", second, left->derive());
			Tree *fourth = new Tree("*", right->derive(), ln);
			Tree *fifth = new Tree("+", third, fourth);
			Tree *sixth = new Tree("*", first, fifth);
			
			delete priortree;
			
			return sixth;
		}
	}
	
	if(priortree->left != NULL)
	{
		priortree->left = priortree->left->derive();
	}
	if(priortree->right != NULL)
	{
		priortree->right = priortree->right->derive();
	}
	
    return priortree;
}

void Tree::print_preorder()
{
    if(var != 0)
    {
        cout << var;
    }
    else if(left == NULL && right == NULL)
    {
        cout << val;
    }
    else if(right == NULL)
    {
        cout << op << "(";
        left->print_preorder();
        cout << ") ";
    }
    else if(op == "-" && left->op == "" && left->val == 0.0)
    {
        cout << "-";
        right->print_preorder();
    }
    else
    {
        cout << "(";
        left->print_preorder();
        cout << " " << op << " ";
        right->print_preorder();
        cout << ")";
    }
}